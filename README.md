# Ikhnos

This project gets waterfall from a given observation and applies an overlay of the signal generated from given TLEs.

# Instructions

Install requirements (better in a virtual environment):

```
pip -r requirements
```

Run the srcipt:

```
usage: ikhnos.py [-h] [-t [TLE_PATH]] [-f [FREQUENCY_OFFSET]] [-r [{24.0,29.0,33.2,38.5,77.0,115.2,230.4}]] [-e [TLE_EPOCH_THRESHOLD]] [-C] [-A] [-W] [-v]
                 observations [observations ...]

Analyze observations

positional arguments:
  observations          One or more observation IDs from SatNOGS Network

optional arguments:
  -h, --help            show this help message and exit
  -t [TLE_PATH], --tle-path [TLE_PATH]
                        Path of the TLE file, if not set the default value is "tle" in the current directory
  -f [FREQUENCY_OFFSET], --frequency-offset [FREQUENCY_OFFSET]
                        Offset in KHz to add to the expected center frequency, this can be negative or positive number, default value: 0.0
  -r [{24.0,29.0,33.2,38.5,77.0,115.2,230.4}], --frequency-range [{24.0,29.0,33.2,38.5,77.0,115.2,230.4}]
                        The plus-minus KHz from the centered frequency as it is in the X axis in observation waterfall, default value: 24.0
  -e [TLE_EPOCH_THRESHOLD], --tle-epoch-threshold [TLE_EPOCH_THRESHOLD]
                        Set the threshold of the difference of observation start time and TLE epoch in days, default value: 2
  -C, --keep-created-files
                        Keep files created by analysis
  -A, --keep-audio      Keep downloaded audio file
  -W, --keep-waterfall  Keep downloaded waterfall file
  -v, --verbose         Be more verbose
```

TLE sets in the file should contain 3 lines.

# Format of the output filenname

<Datetime>_<observation_number>_<TLE_second_line_until_epoch>_<How_long_in_day_fraction_TLE_issued_from_start_time>_<TLE_issued_(A)fter_or_(B)efore_the_start_time>_r<plus-minus KHz from the centered frequency>_f<Offset in KHz added to the center frequency>

Example:

2021-03-25T09:57:13_3838215_0 OBJECT W_0.3642210437037079_A_r33.2_f-5.65

# Acknowledgement

Thanks to cgbsat for the initial script code.
