mutagen==1.45.1
requests==2.25.1
matplotlib==3.4.0
numpy==1.20.1
Pillow==8.1.2
pyephem==9.99
